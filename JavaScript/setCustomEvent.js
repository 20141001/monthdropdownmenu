/**
 * Created by stanimirov on 2/18/2015.
*/
var CUSTOM_EVENT = (function(){

    var msgbox = document.body;
    msgbox.addEventListener("click", SendMessage, false);


// new message: raise newMessage event
    function SendMessage(e) {
        e = e || window.event;
        var event = new CustomEvent("newMessage", {
            detail: {
                inputValue: 'testTEXT',
                importance: 123,
                isDone: true
            },
            bubbles: true,
            cancelable: true
        });
        e.currentTarget.dispatchEvent(event);
    }
})();
