/**
 * Created by stanimirov on 2/19/2015.
 */

var ADD_MONTH_FORM_THE_DROP_DOWN_LIST = (function($){
    'use strict';

     var FEATURE_TESTS = {
            addEventListener: !!window.addEventListener
    },
        VARIABLE_CONTAINER = {
            getTheInputField: $('#month-input-field'),
            getTheSuggestionWindow: $('#suggestionWindow'),
            getTheForm: $('#months-form'),
            getSuggestionAElements: $('.suggestion_window_a'),
            setTimeoutOnKeyUp: 400
        },
        FUNCTION_CONTAINER = {
            captureSuggestionListLiElements: function( callback ){
                if(FEATURE_TESTS.addEventListener){
                    var data  ;
                    $(VARIABLE_CONTAINER.getTheInputField).keyup(function( event ){
                        event = event || window.event;
                        /***Down Keyboard Arrow***/
                        if(event.keyCode != 40) {
                            if (!$(VARIABLE_CONTAINER.getTheSuggestionWindow).hasClass('hideWindow')) {
                                setTimeout(function () {
                                    var aElements = $('.suggestion_window_a');
                                    data = aElements;
                                    if (callback && typeof(callback) === "function") {
                                        // execute the callback, passing parameters as necessary
                                        callback.apply( data );
                                    }
                                }, VARIABLE_CONTAINER.setTimeoutOnKeyUp);
                            }
                        }
                    });
                }
            },
            showSuggestWindow: function(  ){
                VARIABLE_CONTAINER.getTheSuggestionWindow.removeClass('hideWindow');
            },
            hideTheSuggestWindow: function(){
                VARIABLE_CONTAINER.getTheSuggestionWindow.addClass('hideWindow');

            }
        };

 FUNCTION_CONTAINER.captureSuggestionListLiElements(function( data ) {
     data = data || {};
        if(FEATURE_TESTS.addEventListener){
            var getAElements = $('.suggestion_window_a');
            /*******Makes 'a' elements clickable*******/
            $(getAElements).mouseover(function(){
                VARIABLE_CONTAINER.getTheInputField.focusout(FUNCTION_CONTAINER.showSuggestWindow);
            });

            $(getAElements).click(function(){
                $(VARIABLE_CONTAINER.getTheInputField.val($(this).text()));
                /****comes from focusTextToEnd.js module****/
                VARIABLE_CONTAINER.getTheInputField.focusTextToEnd();
                FUNCTION_CONTAINER.hideTheSuggestWindow();
            });

            getAElements.keydown(function( event ){
                /***Down Keyboard Arrow***/
                if( event.keyCode == 40){
                    if(this.parentNode.nextSibling.childNodes !== null){
                        $(this.parentNode.nextSibling.childNodes).addClass('aElementsDecoration').focus();
                        $(this).removeClass('aElementsDecoration');
                    }
                }
                /***Up Keyboard Arrow***/
                if(event.keyCode == 38){
                        if(this.parentNode.previousSibling.childNodes !== null){
                            $(this.parentNode.previousSibling.childNodes).addClass('aElementsDecoration').focus();
                            $(this).removeClass('aElementsDecoration');
                        }
                     }
                /***Keyboard Escape Key***/
                if(event.keyCode == 27){
                    VARIABLE_CONTAINER.getTheInputField.val('').focus();
                    VARIABLE_CONTAINER.getTheSuggestionWindow.addClass('hideWindow');
                    }
                });
        }
});

    /*Return For Tests*/
    var sendForTesting = {
        eListener: FEATURE_TESTS.addEventListener,
        inputFieldCapture: VARIABLE_CONTAINER.getTheInputField,
        suggestionWindowCapture: VARIABLE_CONTAINER.getTheSuggestionWindow,
        formCapture: VARIABLE_CONTAINER.getTheForm
    };

    return sendForTesting;
})(jQuery);