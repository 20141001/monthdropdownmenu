/**
 * Created by stanimirov on 2/12/2015.
 */

module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['JavaScript/*.js'],
                dest: 'concat/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'minified/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'JavaScript/*.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },
        watch: {
            files: ['JavaScript/*.js', 'spec/*.js', 'JQuery/*.js'],
            compass: {
                files: ['sass/*.sass'],
                tasks: ['compass:dev']
            }
            /*Variant 2 for creating dynamic updated css file from sass*/
            /*,
            sass: {
                files: "sass*//*.sass",
                tasks: ['sass']
            }*/
        },
        /*Variant 2 for creating dynamic updated css file from sass*/
       /* sass: {
             dist: {
                 options: {
                     style: 'expanded',
                     update: true
                 },
                 files: {
                     'CSS*//*.css': 'sass*//*.sass'
                 }
             }
         },*/
        jasmine: {
                pivotal: {
                    src: 'JavaScript/*.js',
                    options: {
                        specs: 'spec/*Spec.js',
                        helpers: 'spec/helpers/*.js',
                        vendor: [ "http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js" ]
                    }
                }
        },
        open: {
            build: {
                path: 'http://10.5.0.178/MonthDropDownMenu/_SpecRunner.html',
                app: 'chrome'
            }
        },
        compass: {
            dev: {
                options: {
                    sassDir: ['sass'],
                    cssDir: ['CSS'],
                    environment: 'development'
                },
                prod: {
                    options: {
                        sassDir: ['sass'],
                        cssDir: ['CSS'],
                        environment: 'production'
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-open');

    grunt.registerTask('js', [ 'jasmine']);
    grunt.registerTask('sassy', ['compass:dev', 'watch']);
    grunt.registerTask('jo', [ 'open', 'jasmine']);
    grunt.registerTask('jw', ['watch']);
    grunt.registerTask('jj', ['jshint', 'jasmine']);

    grunt.registerTask('default', ['concat', 'jshint', 'jasmine',  'uglify', 'watch', 'compass:dev']);

};
