/**
 * Created by stanimirov on 2/13/2015.
 */
describe("AUTOCOMPLETE FOR MONTHS", function(){
    var $passFixture,
        $eListener,
        $inputField,
        $suggestionWindow,
        $autocompletePanel,
        $delayTime,
        $monthsArray,
        $arrayToLowerCase,
        $autocompleteIteration,
        $suggestionTimeout;

    beforeEach(function(){
        jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
        $passFixture = loadFixtures("index.html");
        /*Received Data from JS module files*/
        $eListener = AUTOCOMPLETE_FOR_MONTHS.eListener;
        $inputField = $(AUTOCOMPLETE_FOR_MONTHS.inputFieldCapture);
        $suggestionWindow = $(AUTOCOMPLETE_FOR_MONTHS.suggestionWindowCapture);
        $autocompletePanel =  $(AUTOCOMPLETE_FOR_MONTHS.autocompletePannelCapture);
        $delayTime = 400;
        $monthsArray = $(AUTOCOMPLETE_FOR_MONTHS.monthsArrayCapture);
        $arrayToLowerCase = $(AUTOCOMPLETE_FOR_MONTHS.arrayToLowercaseCapture);
        $autocompleteIteration = $(AUTOCOMPLETE_FOR_MONTHS.monthsAutocompleteIterationCapture);
        $suggestionTimeout = $(AUTOCOMPLETE_FOR_MONTHS.suggestionsTimeoutCapture);

    });
    /*Event Listener*/
    describe("Event Listener", function(){
        it("Listener Should Be Defined", function () {
            var specHolder = $eListener;
            expect(specHolder ).toBeDefined();
        });

        it("Listener Should Be Defined", function(){
            var specHolder = $eListener;
            expect(specHolder).toEqual(!!window.addEventListener)
        });

    });
    /*Input Field*/
    describe("Input Field", function(){
        it("Input Field Should Be Defined", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeDefined();
        }) ;

        it("Input Field Should Be In The DOM", function(){
            var specHolder = $inputField;
            expect(specHolder).toBeInDOM();
        });

        it("Input Field Should Be Text Input", function(){
            var specHolder = $inputField;
            expect(specHolder).toHaveAttr("type", "text");
        });
    });
    /*Suggestion Window*/
    describe("Suggestion Window", function(){
        it("Suggestion window Should Be Defined", function(){
            var specHolder = $suggestionWindow;
            expect(specHolder).toBeDefined();
        });

        it("Suggestion Window Should Be In The DOM", function(){
            var specHolder = $suggestionWindow;
            expect(specHolder).toBeInDOM();
        });

        it("Suggestion Window Should Has Class 'hideWindow'", function(){
            var specHolder = $suggestionWindow;
            expect(specHolder).toHaveClass("hideWindow")
        });

        it("Suggestion Window Should Contain UL with ID 'autocomplete-panel'", function(){
            var specHolder = $suggestionWindow;
            expect(specHolder).toContainElement('ul#autocomplete-panel')
        });
    });
    /*Autocomplete Panel*/
    describe("Autocomplete Panel", function(){
        it("Autocomplete Panel Should Be Defined", function(){
            var specHolder = $autocompletePanel;
            expect(specHolder).toBeDefined();
        });

        it("Autocomplete Panel Should Be In The DOM", function(){
            var specHolder = $autocompletePanel;
            expect(specHolder).toBeInDOM();
        });
    });
    /*Suggestion Delay Time*/
    describe("Suggestion Delay Time", function(){
        it("Suggestion Delay Time Should Be Defined", function(){
            var specHolder  = $delayTime;
            expect(specHolder).toBeDefined();
        });

        it("Suggestion Delay Time Should Be 400ms", function(){
            var specHolder  = $delayTime;
            expect(specHolder).toEqual(400);
        });
    });
    /*Month Array*/
    describe("Months Array", function(){
        it("Months Array Should Be Defined", function(){
            var specHolder = $monthsArray;
            expect(specHolder).toBeDefined();
        });

        it("Months Array Should Contain", function(){
            var specHolder = $monthsArray;
            expect(specHolder).toContain('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
        });
    });
    /*To Lower Case*/
    describe("Array Should Be To Lower Case", function(){
        it("Array To Lower Case Should Be Defined", function(){
            var specHolder = $arrayToLowerCase;
            expect(specHolder).toBeDefined();
        });

        it("Array To Lower Case Should Contain", function(){
            var specHolder = $arrayToLowerCase;
            expect(specHolder).toContain('january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');
        });
    });
    /*FUNCTION TESTS*/
    /*Months Autocomplete Iteration*/
    describe("Months Autocomplete Iteration", function(){
        it("Months Autocomplete Iteration Should Be Defined", function(){
            var specHolder  = $autocompleteIteration;
            expect(specHolder).toBeDefined()
        });
    });
    /*Suggestions Timeout*/
    describe("Suggestions Timeout", function(){
        it("Suggestions Timeout Should Be Defined", function(){
            var specHolder = $suggestionTimeout;
            expect(specHolder).toBeDefined()
        });
    });
});