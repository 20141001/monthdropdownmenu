/**
 * Created by stanimirov on 2/13/2015.
 */
describe("SHOW THE SUGGESTION WINDOW",function(){
    var $passFixture, $eListener, $inputField, $suggestionWindow,$theSubmitButton;

beforeEach(function(){
    jasmine.getFixtures().fixturesPath = "../MonthDropDownMenu";
    $passFixture = loadFixtures("index.html");
    /*Received Data from JS module files*/
    $eListener = SHOW_THE_SUGGEST_WINDOW.eListener;
    $inputField = $(SHOW_THE_SUGGEST_WINDOW.inputFieldCapture);
    $suggestionWindow = $(SHOW_THE_SUGGEST_WINDOW.suggestionWindowCapture);
    $theSubmitButton = $('#the-submit-button');
});

/*Event Listener*/
describe("Event Listener", function(){
    it("Listener Should Be Defined", function(){
        var specHolder = $eListener;
        expect(specHolder).toBeDefined();
    });

    it("Listener Should Be Equal To Listener", function(){
        var specHolder = $eListener;
        expect(specHolder).toEqual(!!window.addEventListener);
    });
});

/*Input Field*/
describe("Input Field", function(){
    it("Input Should Be Defined", function(){
        var specHolder = $inputField;
        expect(specHolder).toBeDefined();
    });

    it("input Should Be In The DOM", function(){
        var specHolder = $inputField;
        expect(specHolder).toBeInDOM();
    });
});

/*Suggestion Window*/
describe("Suggestion Window", function(){
    it("Suggestion Window Should Be Described", function(){
        var specHolder = $suggestionWindow;
        expect(specHolder).toBeDefined();
    });

    it("Suggestion Window Should Be In The DOM", function(){
        var specHolder = $suggestionWindow;
        expect(specHolder).toBeInDOM();
    });

    it("Suggestion Window Should Has Class 'hideWindow'", function(){
        var specHolder = $suggestionWindow;
        expect(specHolder).toHaveClass("hideWindow");
    });
});

});